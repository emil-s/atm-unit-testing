# Class diagram

```mermaid
classDiagram
Atm ..> Account
Atm ..> Card
PaymentHandler ..> Account
PaymentHandler ..> Card

class Account {
    -string accountNumber
    -string pin
    -string name
    +double Balance
    +Account(name, balance)
    -GenerateAccountNumber() string
    -GeneratePin() string
}

class PaymentHandler {
    +ValidatePin(account, pin)$ bool
    +ValidateWithdrawalAmount(account, amount)$ bool
    +ValidateDepositAmount(card, amount)$ bool
}
<<abstract>> PaymentHandler

class Atm {
    +AttemptWithdraw(account, card, pin, amount)$ void
    +AttemptDeposit(account, card, pin, amount)$ void
}
<<abstract>> Atm

class Card {
    +double Card
    +Card(balance)
}

```
