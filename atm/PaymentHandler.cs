namespace atm;

public static class PaymentHandler {
    public static bool ValidatePin(Account account, string pin) => account.Pin == pin;
    public static bool ValidateWithdrawalAmount(Account account, double amount) => account.Balance >= amount;
    public static bool ValidateDepositAmount(Card card, double amount) => card.Balance >= amount;
}
