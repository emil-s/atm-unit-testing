namespace atm;

public static class Atm {
    public static void AttemptWithdraw(Account account, Card card, string pin, double amount) {
        if (PaymentHandler.ValidatePin(account, pin)) {
            Console.WriteLine("Wrong pin");
            return;
        }

        if (PaymentHandler.ValidateWithdrawalAmount(account, amount) != true) {
            Console.WriteLine("Insufficient funds");
            return;
        }

        account.Balance -= amount;
        card.Balance += amount;
    }

    public static void AttemptDeposit(Account account, Card card, string pin, double amount) {
        if (PaymentHandler.ValidatePin(account, pin) != true) {
            Console.WriteLine("Wrong pin");
            return;
        }


        if (PaymentHandler.ValidateDepositAmount(card, amount) != true) {
            Console.WriteLine("Insufficient funds");
            return;
        }

        card.Balance -= amount;
        account.Balance += amount;
    }
}
