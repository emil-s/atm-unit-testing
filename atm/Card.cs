namespace atm;

public class Card {
    public Card(double balance) {
        this.balance = balance;
    }

    private double balance;
    public double Balance {
        get => balance;
        set => balance = value;
    }
}
