﻿namespace atm;

public class Account {
	private string accountNumber;
	public string AccountNumber { get => accountNumber; }
	private string pin;
	public string Pin { get => pin; }
	private string name;
	public string Name { get => name; }
	private double balance;
	public double Balance {
		get => balance;
		set => balance = value;
	}

	public Account(string name, double balance) {
		this.accountNumber = GenerateAccountNumber();
		this.pin = GeneratePin();
		this.balance = balance;
		this.name = name;
	}

	private string GenerateAccountNumber() {
		return Random.Shared.Next(100000, 1000000).ToString()
			+ Random.Shared.Next(100000, 1000000).ToString();
	}

	private string GeneratePin() {
		return Random.Shared.Next(100000, 1000000).ToString();
	}

	public bool ValidatePin(Account account, string pin) {
		return account.Pin == pin;
	}
}
