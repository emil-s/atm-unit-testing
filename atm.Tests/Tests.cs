namespace atm.Tests;

public class Testing {
    [Theory]
    [InlineData(10000, 100, 75, 25)]
    [InlineData(1000, 100, 1000, 100)]
    [InlineData(0, 2560, 10, 2550)]
    public void CardBalanceShouldBeEqual(
        double initialAccountBalance,
        double initialCardBalance,
        double deposit,
        double expectedCardBalance) {

        Account testAccount = new("Test User", initialAccountBalance);
        Card testCard = new(initialCardBalance);

        Atm.AttemptDeposit(testAccount, testCard, testAccount.Pin, deposit);

        Assert.Equal(expectedCardBalance, testCard.Balance);
    }

    [Fact]
    public void AccountNumberShouldBeLength() {
        Account testAccount = new("Test User", 1000);
        int expectedLength = 12;

        Assert.Equal(expectedLength, testAccount.AccountNumber.Length);
    }
}
